package database;

import connection.PostgreSQLConnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LineDAO extends PostgreDataDAO implements ILineDAO {
    Logger logger = Logger.getLogger(LineDAO.class.getName());

    private static final String TABLE_MERGE_HELPER = "mergetable";
    public static LineDAO lineDAO;

    public static PostgreDataDAO getInstance() {
        if (lineDAO == null) {
            lineDAO = new LineDAO();
        }
        return lineDAO;
    }

    /**
     * Calls parent constructor and creates a copy of it as outputtable
     **/
    private LineDAO() {
        super();
        CreateOutputTable();
    }

    /**
     * Create the output table, and copy in the values from the input tables.
     **/
    private void CreateOutputTable() {
        String query = "DROP TABLE IF EXISTS public." + TABLE_OUTPUT + ";";
        PostgreSQLConnection.Query(query);
        query = "SELECT * " +
                "INTO " + TABLE_OUTPUT +
                " FROM  " + TABLE_TEST + ";";
        PostgreSQLConnection.Query(query);
    }

    /**
     * Get points with pointDeg degree
     **/
    public long[] getPointByDeg(int pointDeg) {
        CreatePointPool();

        String query =
                "SELECT " + COLUMN_POINT_START + ", count(*)" +
                        " FROM public." + TABLE_MERGE_HELPER +
                        " GROUP BY " + COLUMN_POINT_START +
                        " HAVING count(*) = " + pointDeg + ";";
        long[] temp = getIDS(PostgreSQLConnection.QueryForResult(query));
        DeletePointPool();
        return temp;
    }

    /**
     * Store all the point, occurrence pairs in a helper table, which will be deleted at the end of execution.
     **/
    private void CreatePointPool() {
        DeletePointPool();
        String query = "SELECT " + COLUMN_POINT_START +
                " INTO " + TABLE_MERGE_HELPER +
                " FROM " + TABLE_TEST + ";";
        PostgreSQLConnection.Query(query);
        query = "INSERT INTO " + TABLE_MERGE_HELPER +
                " SELECT " + COLUMN_POINT_END +
                " FROM " + TABLE_TEST + ";";
        PostgreSQLConnection.Query(query);
    }

    /**
     * Delete the helper table from the database.
     **/
    public void DeletePointPool() {
        String query = "DROP TABLE IF EXISTS public." + TABLE_MERGE_HELPER + ";";
        PostgreSQLConnection.Query(query);
    }

    /**
     * Get all the line id-s from resultSet into an array
     **/
    private long[] getIDS(ResultSet resultSet) {
        long[] idStorage = new long[RecordCount(resultSet)];
        try {
            int iterator = 0;
            while (resultSet.next()) {
                idStorage[iterator++] = resultSet.getLong(1);
            }
            resultSet.close();
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
        return idStorage;
    }

    /**
     * Returns the number of records in toCount
     **/
    private int RecordCount(ResultSet toCount) {
        int count = -1;
        try {
            toCount.last();
            count = toCount.getRow();
            toCount.beforeFirst();
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
        return count;
    }

    /**
     * Merges two line-s into one, in other words removes the point which divides the two lines
     * <p>
     * loads the record of two lines into the resultSet variable
     * loads the useful data of the first record into the newStartID variable,
     * and marks the id to be updated.
     * loads the useful data of the second record into the newEndID variable,
     * and marks the id to be deleted.
     * stores the new line data in the record with id: recordIdKeep
     * deletes the outdated line data stored in record with id: recordIDDelete
     **/
    public void RemoveExtraPoint(long idToRemove) {
        String query = "SELECT *" +
                " FROM public." + TABLE_OUTPUT +
                " WHERE (" + COLUMN_POINT_START + " = " + idToRemove + ")" +
                " OR (" + COLUMN_POINT_END + " = " + idToRemove + ");";
        ResultSet resultSet = PostgreSQLConnection.QueryForResult(query);

        long recordIDKeep = -1;
        long recordIDDelete = -1;
        long newStartID = -1;
        long newEndID = -1;

        try {
            resultSet.next();
            newStartID = resultSet.getInt(2) != idToRemove ? resultSet.getInt(2) : resultSet.getInt(3);
            recordIDKeep = resultSet.getInt(1);

            resultSet.next();
            recordIDDelete = resultSet.getInt(1);
            newEndID = resultSet.getInt(2) != idToRemove ? resultSet.getInt(2) : resultSet.getInt(3);


            query = "UPDATE public." + TABLE_OUTPUT +
                    " SET " + COLUMN_POINT_START + " = " + newStartID + ", "
                    + COLUMN_POINT_END + " = " + newEndID +
                    " WHERE " + COLUMN_ID + " = " + recordIDKeep + ";";
            PostgreSQLConnection.Query(query);

            query = "DELETE FROM public." + TABLE_OUTPUT +
                    " WHERE " + COLUMN_ID + " = " + recordIDDelete + ";";
            PostgreSQLConnection.Query(query);

            resultSet.close();
        } catch (SQLException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }
}
