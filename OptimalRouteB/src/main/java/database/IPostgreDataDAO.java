package database;

/**
 * Strings used in PostgreDAO
 **/
public interface IPostgreDataDAO {
    String USERNAME = "postgres";
    String PASSWORD = "alma1234";
    String TABLE_TEST = "testtable";
    String TABLE_OUTPUT = "outtable";
}
