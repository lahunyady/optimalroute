package controller;

import database.PostgreDataDAO;

public abstract class BusinessLogic {
    /**
     * Optimization method to reduces redundancy in database
     **/
    public static void Optimize() {
        /** Get the points with 2 degree **/
        long[] PointsToDelete = PostgreDataDAO.lineDAO.getPointByDeg(2);

        /** Remove the specified points**/
        for (int i = 1; i <= PointsToDelete.length; i++) {
            System.out.println("Progress: " + i + " / " + PointsToDelete.length + " Point Removed");
            PostgreDataDAO.lineDAO.RemoveExtraPoint(PointsToDelete[i - 1]);
        }
        PostgreDataDAO.CloseConnection();
        System.out.println("Done");
    }
}